; Copyright (c) 2015 Joan Bruguera Micó
;
; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.


; Very simple tests for the new (V1.3) instructions of the Chip16 system,
; which are used by very few ROMs, if any, to test basic behaviour.
; If the tests succeed, the screen will turn green.
; If the tests fails, the screen will turn red.

; Recommend running it with a debugger to find where a test fails,
; and to ensure that the end of the tests is reached.

; The NOT, NEG, and REM instructions seem to be implemented consistently
; among the reference emulators (mash16 and RefChip16).
; However, the MOD instruction differs in both emulators, and the documentation
; provides no clue about which one is the valid one (if any).
;
; A, B are 16-bit signed integers, remainder operator (%) as in C99.
;
; mash16's formula is:
;     RESULT = A % B (with C99 semantics)
;     if RESULT < 0:
;         RESULT += B
;
; RefChip16's formula is:
;     RESULT = A % B (with C99 semantics)
;     if SIGN(A) != SIGN(B):
;         RESULT += B
;
; The result of the (%) operator, as in C99, is that it has the same
; sign as the divisor (that is, A). This can be summarized in the
; following table.
;
;  A | B | A % B | MASH16 GOES INSIDE IF? | RC16 GOES INSIDE IF?
; --------------------------------------------------------------
;  + | + |   +   |           NO           |         NO
;  - | + |   -   |          YES           |        YES
;  + | - |   +   |           NO           |        YES
;  - | - |   -   |          YES           |         NO
;
; So if the divisor (that is, B) is negative, both emulators have
; different behaviors.
;
; In order to settle this dispute, I think the best way is to look into
; working implementations that are mathematically sound.
; Haskell has such one (check the `rem` and `mod` operators), and it
; is equivalent to RefChip16's implementation.
; Therefore, this test fails for Mash16 but passes for RefChip16.
;
; Sample Haskell program for verification:
;     main = do
;       print $ (10 `rem` 4)
;       print $ ((-5) `rem` 4)
;       print $ (2 `rem` (-4))
;       print $ ((-2) `rem` (-4))
;       print $ (10 `mod` 4)
;       print $ ((-5) `mod` 4)
;       print $ (2 `mod` (-4))
;       print $ ((-2) `mod` (-4))


start       test11
version     1.3

; Test NOT 0
test11:
  LDI r1, 0
  NOT r1
  CMPI r1, -1
  JNZ failtest

; Test NOT positive number
test12:
  LDI r1, 10
  NOT r1
  CMPI r1, -11
  JNZ failtest

; Test NOT negative number
test13:
  LDI r1, -16
  NOT r1
  CMPI r1, 15
  JNZ failtest

; Test NEG 0
test21:
  LDI r1, 0
  NEG r1
  CMPI r1, 0
  JNZ failtest

; Test NEG positive number
test22:
  LDI r1, 10
  NEG r1
  CMPI r1, -10
  JNZ failtest

; Test NEG negative number
test23:
  LDI r1, -16
  NEG r1
  CMPI r1, 16
  JNZ failtest

; Test REM positive with positive
test31:
  LDI r1, 10
  LDI r2, 4
  REM r1, r2
  CMPI r1, 2
  JNZ failtest

; Test REM negative with positive
test32:
  LDI r1, -5
  LDI r2, 4
  REM r1, r2
  CMPI r1, -1
  JNZ failtest

; Test REM positive with negative
test33:
  LDI r1, 2
  LDI r2, -4
  REM r1, r2
  CMPI r1, 2
  JNZ failtest

; Test REM negative with negative
test34:
  LDI r1, -2
  LDI r2, -4
  REM r1, r2
  CMPI r1, -2
  JNZ failtest

; Test MOD positive with positive
test41:
  LDI r1, 10
  LDI r2, 4
  MOD r1, r2
  CMPI r1, 2
  JNZ failtest

; Test MOD negative with positive
test42:
  LDI r1, -5
  LDI r2, 4
  MOD r1, r2
  CMPI r1, 3
  JNZ failtest

; Test MOD positive with negative
test43:
  LDI r1, 2
  LDI r2, -4
  MOD r1, r2
  CMPI r1, -2
  JNZ failtest

; Test MOD negative with negative
test44:
  LDI r1, -2
  LDI r2, -4
  MOD r1, r2
  CMPI r1, -2
  JNZ failtest

; This is reached if the tests succeed
end:
  BGC 9
  VBLNK
  JMP end

; This is reached if the tests fail
failtest:
  BGC 3
  VBLNK
  JMP failtest
