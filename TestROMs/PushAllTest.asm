; Copyright (c) 2015 Joan Bruguera Micó
;
; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.


; Very simple test for the PUSHALL and POPALL instruction.
; Mostly because the documentation doesn't specify the order of the registers
; (is r0 or r15 pushed first?), so I needed something to test it.
; If the tests succeed, the screen will turn green.
; If the tests fails, the screen will turn red.

; Recommend running it with a debugger to find where a test fails,
; and to ensure that the end of the tests is reached.
;
; Both RefChip16 and mash16 pass this test.


start       pushAllTest
version     0.9

pushAllTest:
  ; Push all registers to the stack with various values with PUSHALL
  LDI r0, 1
  LDI r1, 2
  LDI r2, 3
  LDI r3, 4
  LDI r4, 5
  LDI r5, 6
  LDI r6, 7
  LDI r7, 8
  LDI r8, 9
  LDI r9, 10
  LDI r10, 11
  LDI r11, 12
  LDI r12, 13
  LDI r13, 14
  LDI r14, 15
  LDI r15, 16
  PUSHALL

  ; Pop them all one by one with POP to check the order
  LDI r0, 16
pushAllLoop:
  POP r1
  CMP r1, r0
  JNZ failtest
  SUBI r0, 1
  JNZ pushAllLoop

popAllTest:
  ; Push all the registers to the stack with various values one by one
  LDI r0, 16
popAllLoop:
  PUSH r0
  SUBI r0, 1
  JNZ popAllLoop

  ; Pop all the registers from the stack with POPALL and check the order
  POPALL

  CMPI r0, 16
  JNZ failtest
  CMPI r1, 15
  JNZ failtest
  CMPI r2, 14
  JNZ failtest
  CMPI r3, 13
  JNZ failtest
  CMPI r4, 12
  JNZ failtest
  CMPI r5, 11
  JNZ failtest
  CMPI r6, 10
  JNZ failtest
  CMPI r7, 9
  JNZ failtest
  CMPI r8, 8
  JNZ failtest
  CMPI r9, 7
  JNZ failtest
  CMPI r10, 6
  JNZ failtest
  CMPI r11, 5
  JNZ failtest
  CMPI r12, 4
  JNZ failtest
  CMPI r13, 3
  JNZ failtest
  CMPI r14, 2
  JNZ failtest
  CMPI r15, 1
  JNZ failtest

; This is reached if the tests succeed
end:
  BGC 9
  VBLNK
  JMP end

; This is reached if the tests fail
failtest:
  BGC 3
  VBLNK
  JMP failtest
