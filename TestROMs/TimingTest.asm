; Copyright (c) 2015 Joan Bruguera Micó
;
; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.


; This is a test which checks the timing of VBlanks.
; Since the CPU can't receive any data from the GPU in Chip16,
; bad VBlank time will only have visual effects, but the program can't
; detect it.
; If the tests succeed, the screen will turn green.
; If the tests fails, the screen will turn red.

; While it's unclear how this should exactly work, the documentation states
; that the Chip16 CPU runs at 1000000Hz, and that the refresh rate is 60Hz,
; hence, the number of cycles (=instructions) per VBlank is roughly 16.666,66Hz.
; Mash16 implements this as a frame lasting exactly 16.667 cycles.
; This is what this test aims for, to test this deterministic behavior.

; Recommend running it with a debugger to find where a test fails,
; and to ensure that the end of the tests is reached.
;
; mash16 passes this test.
; RefChip16 has much less predictable cycles-per-frame behavior and doesn't
; pass this test (the number of cycles per frame is variable).


start       start
version     0.9

start:
  ; Here, we are at +0 cycles (start of the program).
  ; Set the background to red + waste 3 cycles for timing purposes.
  BGC 3
  BGC 3
  BGC 3

  ; Here, we are at +3 cycles. We need to waste cycles until +16666 cycles.
wasteCyclesLoop:
  LDI r0, 16662
wasteCyclesLoopCore:
  SUBI r0, 2
  JNZ wasteCyclesLoopCore

  ; Here, we are at +16666 cycles, and the background should be still red.
  ; Set it to green for the final cycle before VBlank so the screen is green.
  BGC 9
  ; Here, we are at +0 cycles again, so set it to red again as soon as possible
  ; in order to detect, + waste 2 cycles for timing purposes
  BGC 3
  BGC 3
  ; Jump to the waiting loop again, making sure to be at +3 cycles after it
  JMP wasteCyclesLoop
