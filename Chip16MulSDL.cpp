/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SDL.h"
#include "SDL_thread.h"
#include <fstream>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include "Chip16/Machine.hpp"

std::vector<u8> readFile(const char *path)
{
	std::ifstream file(path, std::ios::in | std::ios::binary);
	if (!file.is_open())
		throw std::runtime_error("Can't open the ROM file.");

	file.seekg(0, std::ios::end);
	std::streamoff length = file.tellg();
	file.seekg(0, std::ios::beg);

	std::vector<u8> data((size_t)length);
	file.read((char *)&data[0], data.size());
	if (file.fail())
		throw std::runtime_error("Can't read the ROM file.");

	return data;
}

class SDLFrontend : public Chip16::IFrontend
{
	SDL_Surface *screen;
	Chip16::Machine machine;

	SDL_mutex *machineMutex;

	std::vector<s16> audioBuffer;
	size_t audioPos;

public:
	SDLFrontend()
		: screen(NULL), machine(*this), audioBuffer(0), audioPos(0)
	{
		// Initialize SDL
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
			throw std::runtime_error("Error initializing SDL.");

		// Create window
		screen = SDL_SetVideoMode(
			Chip16::GPU::SCREEN_WIDTH, Chip16::GPU::SCREEN_HEIGHT,
			32, SDL_HWSURFACE | SDL_DOUBLEBUF);
		if (screen == NULL)
			throw std::runtime_error("Error setting SDL video mode.");

		SDL_WM_SetCaption("Chip16Mul", NULL);

		// Create machine access mutex
		machineMutex = SDL_CreateMutex();
		if (machineMutex == NULL)
			throw std::runtime_error("Error creating SDL mutex.");

		// Set up the audio callback
		SDL_AudioSpec audio;
		audio.freq = Chip16::SPU::AUDIO_FREQUENCY;
		audio.format = AUDIO_S16SYS;
		audio.channels = 1;
		audio.samples = 10000; // Seems to work best
		audio.callback = AudioCallback;
		audio.userdata = (void *)this;

		if (SDL_OpenAudio(&audio, NULL) != 0)
			throw std::runtime_error("Error setting SDL audio spec.");

		SDL_PauseAudio(0); // Start (unpause) audio
	}

	~SDLFrontend()
	{
		SDL_CloseAudio();
		SDL_DestroyMutex(machineMutex);
		SDL_Quit();
	}

	static const Uint32 FRAME_TIME = 1000 / Chip16::Machine::FRAMES_PER_SECOND;

	void mainLoop()
	{
		const float timePerFrame = 1.0f / Chip16::Machine::FRAMES_PER_SECOND;
		Uint32 prevFrameTime = SDL_GetTicks();
		float timeDelay = 0.0f;

		while (true)
		{
			SDL_Event event;
			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
				case SDL_QUIT:
					return;
				}
			}

			SDL_LockMutex(machineMutex);
			machine.runFrame();
			SDL_UnlockMutex(machineMutex);

			// Cap framerate
			timeDelay += timePerFrame;

			Uint32 ticks = SDL_GetTicks();
			timeDelay -= (ticks - prevFrameTime) / 1000.0f;
			prevFrameTime = ticks;

			if (timeDelay > 0.0f)
				SDL_Delay((Uint32)(timeDelay * 1000));
		}
	}

	void loadRom(u8 *data, size_t size)
	{
		SDL_LockMutex(machineMutex);
		machine.loadRom(data, size);
		SDL_UnlockMutex(machineMutex);
	}

	virtual void renderCallback(u8 *rgbaData)
	{
		// Create a surface and copy it, so we don't have to deal
		// with whatever pixel format SDL decided we need to use.
		// (The endianness handling code comes from the SDL Wiki.)
		/* SDL interprets each pixel as a 32-bit number, so our masks
		   must depend on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		Uint32 Rmask = 0xFF000000;
		Uint32 Gmask = 0x00FF0000;
		Uint32 Bmask = 0x0000FF00;
		Uint32 Amask = 0x000000FF;
#else
		Uint32 Rmask = 0x000000FF;
		Uint32 Gmask = 0x0000FF00;
		Uint32 Bmask = 0x00FF0000;
		Uint32 Amask = 0xFF000000;
#endif

		SDL_Surface *screenSurface = SDL_CreateRGBSurfaceFrom(rgbaData,
			Chip16::GPU::SCREEN_WIDTH, Chip16::GPU::SCREEN_HEIGHT,
			 32, Chip16::GPU::SCREEN_WIDTH * 4, Rmask, Gmask, Bmask, Amask);
		if (screenSurface == NULL)
			return;

		SDL_BlitSurface(screenSurface, NULL, screen, NULL);
		SDL_Flip(screen);
	}

	static void AudioCallback(void *udata, Uint8 *stream, int len)
	{
		SDLFrontend *thisp = (SDLFrontend *)udata;

		size_t nRequested = len / 2;
		size_t requestedPos = 0;

		while (requestedPos != nRequested)
		{
			// If the buffer is empty, then load new data from the machine
			if (thisp->audioBuffer.empty())
			{
				SDL_LockMutex(thisp->machineMutex);
				thisp->audioBuffer = thisp->machine.getSPU().readStream();
				thisp->audioPos = 0;
				SDL_UnlockMutex(thisp->machineMutex);
			}

			// No success in getting more samples -> Fill rest with silence
			if (thisp->audioBuffer.empty())
			{
				break;
			}

			// Write as many samples as we can from the buffer, then silence
			size_t nAvailable = (thisp->audioBuffer.size() - thisp->audioPos);
			size_t nCopy = std::min(nRequested - requestedPos, nAvailable);
			std::memcpy(stream + requestedPos * 2,
				(Uint8 *)&thisp->audioBuffer[thisp->audioPos], nCopy * 2);
			thisp->audioPos += nCopy;
			requestedPos += nCopy;
			if (thisp->audioPos == thisp->audioBuffer.size())
			{
				thisp->audioBuffer.clear();
				thisp->audioPos = 0;
			}
		}

		// Fill the rest of the buffer with silence
		std::memset(stream + requestedPos * 2, 0, (nRequested - requestedPos) * 2);
	}

	virtual void queryInput(u16 &keys1, u16 &keys2)
	{
		Uint8 *kb = SDL_GetKeyState(NULL);

		keys1 = 0;
		if (kb[SDLK_UP])    keys1 |= Chip16::Input::INPUT_UP;
		if (kb[SDLK_DOWN])  keys1 |= Chip16::Input::INPUT_DOWN;
		if (kb[SDLK_LEFT])  keys1 |= Chip16::Input::INPUT_LEFT;
		if (kb[SDLK_RIGHT]) keys1 |= Chip16::Input::INPUT_RIGHT;
		if (kb[SDLK_w])     keys1 |= Chip16::Input::INPUT_SELECT;
		if (kb[SDLK_q])     keys1 |= Chip16::Input::INPUT_START;
		if (kb[SDLK_x])     keys1 |= Chip16::Input::INPUT_A;
		if (kb[SDLK_z])     keys1 |= Chip16::Input::INPUT_B;

		keys2 = 0;
		if (kb[SDLK_KP8])   keys2 |= Chip16::Input::INPUT_UP;
		if (kb[SDLK_KP2])   keys2 |= Chip16::Input::INPUT_DOWN;
		if (kb[SDLK_KP4])   keys2 |= Chip16::Input::INPUT_LEFT;
		if (kb[SDLK_KP6])   keys2 |= Chip16::Input::INPUT_RIGHT;
		if (kb[SDLK_KP9])   keys2 |= Chip16::Input::INPUT_SELECT;
		if (kb[SDLK_KP7])   keys2 |= Chip16::Input::INPUT_START;
		if (kb[SDLK_KP1])   keys2 |= Chip16::Input::INPUT_A;
		if (kb[SDLK_KP3])   keys2 |= Chip16::Input::INPUT_B;
	}
};

int main(int argc, char *argv[])
{
	try
	{
		if (argc != 2)
		{
			std::cout << "Usage: " << argv[0] << " rom.c16" << std::endl;
			return EXIT_FAILURE;
		}

		std::vector<u8> rom = readFile(argv[1]);

		SDLFrontend frontend;
		frontend.loadRom(&rom[0], rom.size());
		frontend.mainLoop();

		return EXIT_SUCCESS;
	}
	catch (const std::exception &ex)
	{
		std::cerr << ex.what() << std::endl;

		return EXIT_FAILURE;
	}
}
