/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CHIP16_INPUT_HPP
#define CHIP16_INPUT_HPP

#include "Machine.hpp"

namespace Chip16
{
	/**
	  * The input device of a Chip16 machine.
	  */
	class Input
	{
		Machine &machine;

	public:
		/**
		  * Create an input device.
		  * @param machine The machine which contains the input device.
		  */
		Input(Machine &machine);

		/**
		  * Set the input device to its initial state.
		  */
		void reset();

		/**
		  * Check which buttons are pressed (calls the frontend).
		  * @remarks The result will be written to the IO ports of the memory.
		  */
		void query();

		/**
		  * Bits for specifying which buttons are pressed.
		  */
		enum InputFlags
		{
			INPUT_UP = 1 << 0,
			INPUT_DOWN = 1 << 1,
			INPUT_LEFT = 1 << 2,
			INPUT_RIGHT = 1 << 3,
			INPUT_SELECT = 1 << 4,
			INPUT_START = 1 << 5,
			INPUT_A = 1 << 6,
			INPUT_B = 1 << 7
		};
	};
}

#endif // CHIP16_INPUT_HPP
