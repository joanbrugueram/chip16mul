/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Machine.hpp"
#include <vector>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <stdexcept>

// Various prefedined durations (in milliseconds) for wave generation
const static double waveAttackTimes[16] = {
	2/1000.0, 8/1000.0, 16/1000.0, 24/1000.0, 38/1000.0, 56/1000.0, 68/1000.0,
	80/1000.0, 100/1000.0, 250/1000.0, 500/1000.0, 800/1000.0, 1000/1000.0,
	3000/1000.0, 5000/1000.0, 8000/1000.0
};

const static double waveDecayTimes[16] = {
	6/1000.0, 24/1000.0, 48/1000.0, 72/1000.0, 114/1000.0, 168/1000.0, 204/1000.0,
	240/1000.0, 300/1000.0, 750/1000.0, 1500/1000.0, 2400/1000.0, 3000/1000.0,
	9000/1000.0, 15000/1000.0, 24000/1000.0
};

const static double waveReleaseTimes[16] = {
	6/1000.0, 24/1000.0, 48/1000.0, 72/1000.0, 114/1000.0, 168/1000.0, 204/1000.0,
	240/1000.0, 300/1000.0, 750/1000.0, 1500/1000.0, 2400/1000.0, 3000/1000.0,
	9000/1000.0, 15000/1000.0, 24000/1000.0
};

Chip16::SPU::SPU(Machine &machine)
	: machine(machine)
{
}

void Chip16::SPU::reset()
{
	lastStreamTime = 0.0;
	stream.clear();

	stateWaveType = SoundWaveType_Triangle;
	stateAttackTime = 0.0;
	stateDecayTime = 0.0;
	stateReleaseTime = 0.0;
	statePeakVolume = 0.0;
	stateSustainVolume = 0.0;

	playing = false;
	playStartTime = 0.0;
	playDuration = 0.0;
	playFrequency = 0;
	playWaveType = SoundWaveType_Triangle;
	playAttackTime = 0.0;
	playDecayTime = 0.0;
	playReleaseTime = 0.0;
	playPeakVolume = 0.0;
	playSustainVolume = 0.0;

	randomSample = 0;
	randomSampleTime = 0.0;
}

void Chip16::SPU::playTone(int frequency, double time)
{
	updateStream();

	if (playing && playWaveType == SoundWaveType_Sine && playFrequency == frequency)
	{
		// Some ROMs love to spam the SND opcode before the sound has ended
		// playing, which causes "choppy" sound because the sine wave is constantly
		// being cut. In this special case, don't reset the sine wave so the
		// result is a clear sine wave.
		playDuration = time;
	}
	else
	{
		// Start playing a new sine wave. Attack/decay/etc. do not need to be set
		playing = true;
		playStartTime = machine.getRunningTime();
		playDuration = time;
		playFrequency = frequency;
		playWaveType = SoundWaveType_Sine;
		playAttackTime = 0.0;
		playDecayTime = 0.0;
		playReleaseTime = 0.0;
		playPeakVolume = 0.0;
		playSustainVolume = 1.0;
		randomSample = 0;
		randomSampleTime = 0.0;
	}
}

void Chip16::SPU::setWaveParameters(size_t waveType, size_t attackTimeIndex,
	size_t decayTimeIndex, size_t releaseTimeIndex, size_t peakVolumeIndex,
	size_t sustainVolumeIndex)
{
	// This sets the hidden SPU state without affecting the currently playing wave
	stateWaveType = (SoundWaveType)waveType;
	stateAttackTime = waveAttackTimes[attackTimeIndex];
	stateDecayTime = waveDecayTimes[decayTimeIndex];
	stateReleaseTime = waveReleaseTimes[releaseTimeIndex];
	statePeakVolume = ((double)peakVolumeIndex / 0xF);
	stateSustainVolume = ((double)sustainVolumeIndex / 0xF);
}

void Chip16::SPU::playWave(int frequency, double time)
{
	updateStream();

	// Start playing the new wave. Parameters are dumped from hidden SPU
	// state to currently playing state
	playing = true;
	playStartTime = machine.getRunningTime();
	playDuration = time + stateReleaseTime;
	playFrequency = frequency;
	playWaveType = stateWaveType;
	playAttackTime = stateAttackTime;
	playDecayTime = stateDecayTime;
	playReleaseTime = stateReleaseTime;
	playPeakVolume = statePeakVolume;
	playSustainVolume = stateSustainVolume;
	randomSample = 0;
	randomSampleTime = 0.0;
}

void Chip16::SPU::stop()
{
	updateStream();

	playing = false;
}

void Chip16::SPU::updateStream()
{
	double currentTime = machine.getRunningTime();
	double (SPU::*sampleGenerationFunc)(double);

	if (playing)
	{
		double endTimeToPlay = std::min(playStartTime + playDuration, currentTime);

		size_t nSamples = (size_t)((endTimeToPlay - lastStreamTime) * AUDIO_FREQUENCY);

		// Decide sample generation function
		switch (playWaveType)
		{
			case SoundWaveType_Sine:
				sampleGenerationFunc = &SPU::generateSineSample;
				break;
			case SoundWaveType_Triangle:
				sampleGenerationFunc = &SPU::generateTriangleSample;
				break;
			case SoundWaveType_Sawtooth:
				sampleGenerationFunc = &SPU::generateSawtoothSample;
				break;
			case SoundWaveType_Pulse:
				sampleGenerationFunc = &SPU::generatePulseSample;
				break;
			case SoundWaveType_Noise:
				sampleGenerationFunc = &SPU::generateNoiseSample;
				break;
			default:
				throw std::runtime_error(format("Internal error: Wave type = %d is not defined.", playWaveType));
		}

		// Generate samples using sample generation function
		for (size_t i = 0; i < nSamples; i++)
		{
			double time = (lastStreamTime - playStartTime) + (float)i / (float)AUDIO_FREQUENCY;
			double volume = computeAdsrVolume(time);
			s16 sample = (s16)((this->*sampleGenerationFunc)(time) * volume * 0x7FFF);
			stream.push_back(sample);
		}

		lastStreamTime += (double)nSamples / AUDIO_FREQUENCY;
		if (endTimeToPlay == playStartTime + playDuration)
		{
			playing = false;
		}
	}

	// Generate silence for the rest of the time
	size_t nSamples = (size_t)((currentTime - lastStreamTime) * AUDIO_FREQUENCY);
	for (size_t i = 0; i < nSamples; i++)
	{
		stream.push_back(0);
	}

	lastStreamTime += (double)nSamples / AUDIO_FREQUENCY;
}

std::vector<s16> Chip16::SPU::readStream()
{
	updateStream();

	std::vector<s16> streamCpy = stream;
	stream.clear();
	return streamCpy;
}

static inline double linearScale(double x, double r1s, double r1e, double r2s, double r2e)
{
	double normalized = ((x - r1s) / (r1e - r1s));
	return r2s + normalized * (r2e-r2s);
}

double Chip16::SPU::generateSineSample(double time)
{
	return std::sin(time * 2.0f * 3.14159265f * playFrequency);
}

double Chip16::SPU::generateTriangleSample(double time)
{
	double percentInIteration = fmod(time, 1.0 / playFrequency) * playFrequency;
	if (percentInIteration <= 0.5)
		return linearScale(percentInIteration, 0.0, 0.5, -1.0, 1.0);
	else
		return linearScale(percentInIteration, 0.5, 1.0, 1.0, -1.0);
}

double Chip16::SPU::generateSawtoothSample(double time)
{
	double percentInIteration = fmod(time, 1.0 / playFrequency) * playFrequency;
	return linearScale(percentInIteration, 0.0, 1.0, -1.0, 1.0);
}

double Chip16::SPU::generatePulseSample(double time)
{
	double percentInIteration = fmod(time, 1.0 / playFrequency) * playFrequency;
	return (percentInIteration <= 0.5) ? -1.0 : 1.0;
}

double Chip16::SPU::generateNoiseSample(double time)
{
	if ((time - randomSampleTime) < 1.0/playFrequency)
	{
		// Pull new random sample
		// Not guaranteed to be 100% uniform but should do the trick+
		// The RNG is seeded in the machine reset code
		randomSample = (double)std::rand() / RAND_MAX;
		if (std::rand() <= RAND_MAX/2)
			randomSample = -randomSample;

		randomSampleTime += 1.0/playFrequency;
	}

	return randomSample;
}

double Chip16::SPU::computeAdsrVolume(double time)
{
	if (time < playAttackTime)
	{
		// In attack phase, linear ramp from 0 to peak intensity
		return linearScale(time, 0.0, playAttackTime, 0.0, playPeakVolume);
	}
	else if (time < playAttackTime + playDecayTime)
	{
		// In decay phase, linear ramp from peak to sustain intensity
		return linearScale(time, playAttackTime, playAttackTime + playDecayTime,
			playPeakVolume, playSustainVolume);
	}
	else if (time >= playDuration - playReleaseTime)
	{
		// In release phase, linear ramp from sustain to 0
		return linearScale(time, playDuration - playReleaseTime, playDuration,
			playSustainVolume, 0.0);
	}
	else
	{
		// In sustain phase, keep sustain volume
		return playSustainVolume;
	}
}
