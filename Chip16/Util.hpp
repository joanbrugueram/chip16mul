/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
// Fixed size types
#ifdef _MSC_VER
typedef signed __int8 s8;
typedef unsigned __int8 u8;
typedef signed __int16 s16;
typedef unsigned __int16 u16;
typedef signed __int32 s32;
typedef unsigned __int32 u32;
#else
#include <stdint.h>
typedef int8_t s8;
typedef uint8_t u8;
typedef int16_t s16;
typedef uint16_t u16;
typedef int32_t s32;
typedef uint32_t u32;
#endif

#include <string>

/**
  * Create an std::string with a printf-like syntax.
  * @param fmt The format specified string.
  * @return The formatted string.
  */
std::string format(const char *fmt, ...);
