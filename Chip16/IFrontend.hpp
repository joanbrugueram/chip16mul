/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CHIP16_FRONTEND_H
#define CHIP16_FRONTEND_H

#include "Machine.hpp"

namespace Chip16
{
	/**
	  * An interface to be implemented by front-end applications
	  * to handle input/output operations from the underlying OS.
	  */
	class IFrontend
	{
	public:
		virtual ~IFrontend() { }

		/**
		  * Called when a frame is available to be rendered.
		  * @param rgbData Screen in RGBA8888 format (size is WIDTH*HEIGHT*4).
		  * @remarks See Chip16::GPU for SCREEN_WIDTH and SCREEN_HEIGHT.
		  */
		virtual void renderCallback(u8 *rgbaData) = 0;

		/**
		  * Called when the emulator wants to recieve user input.
		  * @param keys1 Key bitset for player 1, to be filled by the program.
		  * @param keys2 Key bitset for player 2, to be filled by the program.
		  * @remarks See Chip16::Input for INPUT_* bits.
		  */
		virtual void queryInput(u16 &keys1, u16 &keys2) = 0;
	};
}

#endif // CHIP16_FRONTEND_H
