/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Machine.hpp"
#include <cstring>
#include <stdexcept>

Chip16::Memory::Memory(Chip16::Machine &machine)
	: machine(machine)
{
}

void Chip16::Memory::reset()
{
	std::memset(data, 0, sizeof(data));
}

void Chip16::Memory::load(u16 address, u8 *bytes, size_t length) const
{
	if (address + length > sizeof(data))
		throw std::out_of_range("Chip16 Memory: Out of bounds.");

	std::memcpy(bytes, &data[address], length);
}

void Chip16::Memory::store(u16 address, const u8 *bytes, size_t length)
{
	if (address + length > sizeof(data))
		throw std::out_of_range("Chip16 Memory: Out of bounds.");

	std::memcpy(&data[address], bytes, length);
}
