/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Util.hpp"

#include <cstdio>
#include <cstdarg>
#include <vector>

std::string format(const char *fmt, ...)
{
	// Figure out the buffer size we need in order to store the string
	va_list args;
	va_start(args, fmt);
	#ifndef _MSC_VER
		int size = vsnprintf(NULL, 0, fmt, args);
	#else
		// Visual C++ has a broken snprintf implementation which returns -1
		// if the arguments don't fit in the buffer instead of the required size.
		// Fortunately, a _vscprintf is provided just for that purpose.
		int size = _vscprintf(fmt, args);
	#endif
	va_end(args);

	// Format the string into a std::vector (we can't use std::string directly
  // because in C++03 it doesn't have a contiguity requirement)
	std::vector<char> strBuffer(size + 1);

	va_start(args, fmt);
	vsnprintf(&strBuffer[0], strBuffer.size(), fmt, args);
	va_end(args);

	// Finally, convert the result to an string
	return std::string(&strBuffer[0]);
}
