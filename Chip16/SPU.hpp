/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CHIP16_SPU_HPP
#define CHIP16_SPU_HPP

#include "Machine.hpp"
#include <vector>

namespace Chip16
{
	/**
	  * The sound processing unit of a Chip16 machine.
	  */
	class SPU
	{
	public:
		/**
		  * Frequency of the audio given to the frontend.
		  */
		static const int AUDIO_FREQUENCY = 44100;

		/**
		  * Create a sound processing unit.
		  * @param machine The machine that contains the sound processing unit.
		  */
		SPU(Machine &machine);

		/**
		  * Set the sound processing unit to its initial state.
		  */
		void reset();

		/**
		  * Play a constant tone of the specified frequency for the specified time.
		  * @param frequency The frequency of the tone.
		  * @param time The time to play the tone, in seconds.
		  */
		void playTone(int frequency, double time);

		/**
		  * Set wave generation parameters.
			* @param attackTimeIndex One of the 16 predefined attack times [0-15].
			* @param decayTimeIndex One of the 16 predefined decay times [0-15].
			* @param peakVolumeIndex One of the 16 predefined peak volumes [0-15].
			* @param waveType One of the 4 predefined wave types [0-3].
			* @param sustainVolumeIndex One of the 16 predefined sustain volumes [0-15].
			* @param releaseTimeIndex One of the 16 predefined release times [0-15].
			*/
		void setWaveParameters(size_t waveType, size_t attackTimeIndex,
			size_t decayTimeIndex, size_t releaseTimeIndex, size_t peakVolumeIndex,
			size_t sustainVolumeIndex);

		/**
		  * Play a wave of the specified frequency for the specified time.
			* The generated wave also depends on the wave parameters previously set.
		  * @param frequency The frequency of the tone.
		  * @param time The time to play the tone, in seconds.
		  */
		void playWave(int frequency, double time);

		/**
		  * Stop playing any sound.
		  */
		void stop();

		/**
		  * Update the SPU sample buffer.
		  */
		void updateStream();

		/**
		  * Read the contents of the SPU sample buffer and empty it.
		  */
		std::vector<s16> readStream();

	private:
		Machine &machine;

		/**
		  * Time of the last update of the SPU sample buffer.
		  */
		double lastStreamTime;

		/**
		  * Sample queue to be played.
		  */
		std::vector<s16> stream;

		/**
		  * Possible sound wave types that the SPU can play.
			*/
		enum SoundWaveType
		{
			SoundWaveType_Sine = -1,
			SoundWaveType_Triangle = 0,
			SoundWaveType_Sawtooth = 1,
			SoundWaveType_Pulse = 2,
			SoundWaveType_Noise = 3,
		};

		/**
		  * Sound wave parameters set by the SNG command.
			* Those are stored here until the SNP command is used.
			*/
		SoundWaveType stateWaveType;
		double stateAttackTime;
		double stateDecayTime;
		double stateReleaseTime;
		double statePeakVolume;
		double stateSustainVolume;

		/**
		  * True if we are currently playing.
			*/
		bool playing;

		/**
		  * If playing, the starting (machine) time of the sound, in seconds.
			*/
		double playStartTime;

		/**
		  * If playing, the ending (machine) time of the sound, in seconds.
			*/
		double playDuration;

		/**
		  * If playing, the frequency (in Hz) of the sound.
			*/
		int playFrequency;

		/**
		  * Sound wave parameters for the currently played wave.
			*/
		SoundWaveType playWaveType;
		double playAttackTime;
		double playDecayTime;
		double playReleaseTime;
		double playPeakVolume;
		double playSustainVolume;

		/**
		  * Current random sample for the noise wave.
			*/
		double randomSample;

		/**
		  * (Ideal) time when the current random sample for the noise wave was generated.
			*/
		double randomSampleTime;

		/**
		  * Sample generation functions. Generate a sample in range -1.0 to 1.0
			* for the various wave functions.
			* @param Time since the start of the sound command.
			*/
		double generateSineSample(double time);
		double generateTriangleSample(double time);
		double generateSawtoothSample(double time);
		double generatePulseSample(double time);
		double generateNoiseSample(double time);

		/**
		  * Compute the Attack-Decay-Sustain-Release volume at a point in time.
			* @param Time since the start of the sound command.
			*/
		double computeAdsrVolume(double time);
	};
}

#endif // CHIP16_SPU_HPP
