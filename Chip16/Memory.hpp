/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CHIP16_MEMORY_HPP
#define CHIP16_MEMORY_HPP

#include "Machine.hpp"

namespace Chip16
{
	/**
	  * The memory chip of a Chip16 machine.
	  */
	class Memory
	{
		Machine &machine;
		u8 data[0x10000];

		/* The usual layout of the memory is:
		 * 0x0000 to 0xFDF0: ROM data.
		 * 0xFDF0 to 0xFFF0: Stack.
		 * 0xFFF0 to 0x10000: IO ports.
		 *
		 * However, this isn't enforced by the spec or the emulator.
		 */

	public:
		/**
		  * Create a memory chip.
		  * @param machine The machine that contains the memory chip.
		  */
		Memory(Machine &machine);

		/**
		  * Set the memory to its initial state.
		  */
		void reset();

		/**
		  * Read bytes directly from the memory.
		  * @param address The address of the memory to read from.
		  * @param bytes The pointer which will recieve the data.
		  * @param length The number of bytes to read.
		  */
		void load(u16 address, u8 *bytes, size_t length) const;

		/**
		  * Read a value of type T from the memory.
		  * @param address The address of the memory to read from.
		  * @return The value.
		  */
		template<typename T>
		T load(u16 address) const
		{
			u8 bytes[sizeof(T)];
			load(address, bytes, sizeof(bytes));

			T var = 0;
			for (size_t i = 0; i < sizeof(T); i++)
				var = (var << 8) | bytes[sizeof(T)-i-1];
			return var;
		}

		/**
		  * Store bytes directly to the memory.
		  * @param address The address of the memory to write to.
		  * @param bytes The pointer which contains the data.
		  * @param length The number of bytes to write.
		  */
		void store(u16 address, const u8 *bytes, size_t length);

		/**
		  * Store a value of type T to the memory.
		  * @param address The address of the memory to write to.
		  * @param var The value to write.
		  */
		template<typename T>
		void store(u16 address, T var)
		{
			u8 bytes[sizeof(T)];
			for (size_t i = 0; i < sizeof(T); i++)
			{
				bytes[i] = var & 0xFF;
				var >>= 8;
			}

			store(address, (u8 *)bytes, sizeof(bytes));
		}
	};
}

#endif // CHIP16_MEMORY_HPP
