/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Machine.hpp"
#include <cstdlib>
#include <stdexcept>

std::string Chip16::CPU::Instruction::toString()
{
	static const char *regNames[] = {
		"r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7",
		"r8", "r9", "ra", "rb", "rc", "rd", "re", "rf"
	};

	static const char *conditionalNames[] = {
		"Z", "NZ", "N", "NN", "P", "O", "NO", "A",
		"AE", "B", "BE", "G", "GE", "L", "LE", "RES"
	};

#define REGX regNames[regX()]
#define REGY regNames[regY()]
#define REGZ regNames[regZ()]
#define IMM immediate()
#define IMM8 immediate8()
#define LOW low()
#define HIGH high()
#define AMOUNT N()
#define CONDITIONALTYPE conditionalNames[conditionalType()]

	switch (opcode())
	{
	case OPCODE_NOP:   return format("NOP");
	case OPCODE_CLS:   return format("CLS");
	case OPCODE_VBLNK: return format("VBLNK");
	case OPCODE_BGC:   return format("BGC %.1X", AMOUNT);
	case OPCODE_SPR:   return format("SPR %.4X", IMM);
	case OPCODE_DRW:   return format("DRW %s, %s, %.4X", REGX, REGY, IMM);
	case OPCODE_DRW_:  return format("DRW %s, %s, %s", REGX, REGY, REGZ);
	case OPCODE_RND:   return format("RND %s, %.4X", REGX, IMM);

	case OPCODE_FLIP:  return format("FLIP %d", HIGH);

	case OPCODE_SND0:  return format("SND0");
	case OPCODE_SND1:  return format("SND1 %.4X", IMM);
	case OPCODE_SND2:  return format("SND2 %.4X", IMM);
	case OPCODE_SND3:  return format("SND3 %.4X", IMM);
	case OPCODE_SNP:   return format("SNP %s, %.4X", REGX, IMM);
	case OPCODE_SNG:   return format("SNG %.2X, %.4X", IMM8, IMM);

	case OPCODE_JMPI:  return format("JMP @%.4X", IMM);
	case OPCODE_JMC:   return format("JMC @%.4X", IMM);
	case OPCODE_Jx:    return format("J%s @%.4X", CONDITIONALTYPE, IMM);
	case OPCODE_JME:   return format("JME %s, %s, @%.4X", REGX, REGY, IMM);
	case OPCODE_CALLI: return format("CALL @%.4X", IMM);
	case OPCODE_RET:   return format("RET");
	case OPCODE_JMP:   return format("JMP @%s", REGX);
	case OPCODE_Cx:    return format("C%s @%.4X", CONDITIONALTYPE, IMM);
	case OPCODE_CALL:  return format("CALL @%s", REGX);

	case OPCODE_LDI:   return format("LDI %s, %.4X", REGX, IMM);
	case OPCODE_LDISP: return format("LDI SP, %.4X", IMM);
	case OPCODE_LDMI:  return format("LDM %s, %.4X", REGX, IMM);
	case OPCODE_LDM:   return format("LDM %s, %s", REGX, REGY);
	case OPCODE_MOV:   return format("MOV %s, %s", REGX, REGY);

	case OPCODE_STMI:  return format("STM %s, %.4X", REGX, IMM);
	case OPCODE_STM:   return format("STM %s, %s", REGX, REGY);

	case OPCODE_ADDI:  return format("ADDI %s, %.4X", REGX, IMM);
	case OPCODE_ADD:   return format("ADD %s, %s", REGX, REGY);
	case OPCODE_ADD_:  return format("ADD %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_SUBI:  return format("SUBI %s, %.4X", REGX, IMM);
	case OPCODE_SUB:   return format("SUB %s, %s", REGX, REGY);
	case OPCODE_SUB_:  return format("SUB %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_CMPI:  return format("CMPI %s, %.4X", REGX, IMM);
	case OPCODE_CMP:   return format("CMP %s, %s", REGX, REGY);

	case OPCODE_ANDI:  return format("ANDI %s, %.4X", REGX, IMM);
	case OPCODE_AND:   return format("AND %s, %s", REGX, REGY);
	case OPCODE_AND_:  return format("AND %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_TSTI:  return format("TSTI %s, %.4X", REGX, IMM);
	case OPCODE_TST:   return format("TST %s, %s", REGX, REGY);

	case OPCODE_ORI:   return format("ORI %s, %.4X", REGX, IMM);
	case OPCODE_OR:    return format("OR %s, %s", REGX, REGY);
	case OPCODE_OR_:   return format("OR %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_XORI:  return format("XORI %s, %.4X", REGX, IMM);
	case OPCODE_XOR:   return format("XOR %s, %s", REGX, REGY);
	case OPCODE_XOR_:  return format("XOR %s, %s", REGX, REGY, REGZ);

	case OPCODE_MULI:  return format("MULI %s, %.4X", REGX, IMM);
	case OPCODE_MUL:   return format("MUL %s, %s", REGX, REGY);
	case OPCODE_MUL_:  return format("MUL %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_DIVI:  return format("DIVI %s, %.4X", REGX, IMM);
	case OPCODE_DIV:   return format("DIV %s, %s", REGX, REGY);
	case OPCODE_DIV_:  return format("DIV %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_MODI:  return format("MODI %s, %.4X", REGX, IMM);
	case OPCODE_MOD:   return format("MOD %s, %s", REGX, REGY);
	case OPCODE_MOD_:  return format("MOD %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_REMI:  return format("REMI %s, %.4X", REGX, IMM);
	case OPCODE_REM:   return format("REM %s, %s", REGX, REGY);
	case OPCODE_REM_:  return format("REM %s, %s, %s", REGX, REGY, REGZ);

	case OPCODE_SHLI:  return format("SHL %s, %.1X", REGX, AMOUNT);
	case OPCODE_SHRI:  return format("SHR %s, %.1X", REGX, AMOUNT);
	case OPCODE_SARI:  return format("SAR %s, %.1X", REGX, AMOUNT);
	case OPCODE_SHL:   return format("SHL %s, %s", REGX, REGY);
	case OPCODE_SHR:   return format("SHR %s, %s", REGX, REGY);
	case OPCODE_SAR:   return format("SAR %s, %s", REGX, REGY);

	case OPCODE_PUSH:  return format("PUSH %s", REGX);
	case OPCODE_POP:   return format("POP %s", REGX);
	case OPCODE_PUSHALL: return format("PUSHALL");
	case OPCODE_POPALL:  return format("POPALL");
	case OPCODE_PUSHF: return format("PUSHF");
	case OPCODE_POPF:  return format("POPF");

	case OPCODE_PAL:   return format("PAL %.4X", IMM);
	case OPCODE_PAL_:  return format("PAL %s", REGX);

	case OPCODE_NOTI:  return format("NOTI %s, %.4X", REGX, IMM);
	case OPCODE_NOT:   return format("NOT %s", REGX);
	case OPCODE_NOT_:  return format("NOT %s, %s", REGX, REGY);

	case OPCODE_NEGI:  return format("NEGI %s, %.4X", REGX, IMM);
	case OPCODE_NEG:   return format("NEG %s", REGX);
	case OPCODE_NEG_:  return format("NEG %s, %s", REGX, REGY);

	default:           return format("DB %.8X???", value);
	}

#undef REGX
#undef REGY
#undef REGZ
#undef IMM
#undef IMM8
#undef LOW
#undef HIGH
#undef AMOUNT
#undef CONDITIONALTYPE
}

Chip16::CPU::CPU(Machine &machine)
	: machine(machine)
{
	// Declare minimum version of the machine required to execute each opcode
	std::fill(opcodesMinVersion, opcodesMinVersion + sizeof(opcodesMinVersion), 0);
	opcodesMinVersion[OPCODE_NOP] = 0x08;
	opcodesMinVersion[OPCODE_CLS] = 0x08;
	opcodesMinVersion[OPCODE_VBLNK] = 0x08;
	opcodesMinVersion[OPCODE_BGC] = 0x08;
	opcodesMinVersion[OPCODE_SPR] = 0x08;
	opcodesMinVersion[OPCODE_DRW] = 0x08;
	opcodesMinVersion[OPCODE_DRW_] = 0x08;
	opcodesMinVersion[OPCODE_RND] = 0x08;
	opcodesMinVersion[OPCODE_FLIP] = 0x08;
	opcodesMinVersion[OPCODE_SND0] = 0x08;
	opcodesMinVersion[OPCODE_SND1] = 0x08;
	opcodesMinVersion[OPCODE_SND2] = 0x08;
	opcodesMinVersion[OPCODE_SND3] = 0x08;
	opcodesMinVersion[OPCODE_SNP] = 0x11;
	opcodesMinVersion[OPCODE_SNG] = 0x11;
	opcodesMinVersion[OPCODE_JMPI] = 0x08;
	opcodesMinVersion[OPCODE_JMC] = 0x08;
	opcodesMinVersion[OPCODE_Jx] = 0x09;
	opcodesMinVersion[OPCODE_JME] = 0x08;
	opcodesMinVersion[OPCODE_CALLI] = 0x08;
	opcodesMinVersion[OPCODE_RET] = 0x08;
	opcodesMinVersion[OPCODE_JMP] = 0x08;
	opcodesMinVersion[OPCODE_Cx] = 0x09;
	opcodesMinVersion[OPCODE_CALL] = 0x08;
	opcodesMinVersion[OPCODE_LDI] = 0x08;
	opcodesMinVersion[OPCODE_LDISP] = 0x08;
	opcodesMinVersion[OPCODE_LDMI] = 0x08;
	opcodesMinVersion[OPCODE_LDM] = 0x08;
	opcodesMinVersion[OPCODE_MOV] = 0x08;
	opcodesMinVersion[OPCODE_STMI] = 0x08;
	opcodesMinVersion[OPCODE_STM] = 0x08;
	opcodesMinVersion[OPCODE_ADDI] = 0x08;
	opcodesMinVersion[OPCODE_ADD] = 0x08;
	opcodesMinVersion[OPCODE_ADD_] = 0x08;
	opcodesMinVersion[OPCODE_SUBI] = 0x08;
	opcodesMinVersion[OPCODE_SUB] = 0x08;
	opcodesMinVersion[OPCODE_SUB_] = 0x08;
	opcodesMinVersion[OPCODE_CMPI] = 0x08;
	opcodesMinVersion[OPCODE_CMP] = 0x08;
	opcodesMinVersion[OPCODE_ANDI] = 0x08;
	opcodesMinVersion[OPCODE_AND] = 0x08;
	opcodesMinVersion[OPCODE_AND_] = 0x08;
	opcodesMinVersion[OPCODE_TSTI] = 0x08;
	opcodesMinVersion[OPCODE_TST] = 0x08;
	opcodesMinVersion[OPCODE_ORI] = 0x08;
	opcodesMinVersion[OPCODE_OR] = 0x08;
	opcodesMinVersion[OPCODE_OR_] = 0x08;
	opcodesMinVersion[OPCODE_XORI] = 0x08;
	opcodesMinVersion[OPCODE_XOR] = 0x08;
	opcodesMinVersion[OPCODE_XOR_] = 0x08;
	opcodesMinVersion[OPCODE_MULI] = 0x11;
	opcodesMinVersion[OPCODE_MUL] = 0x08;
	opcodesMinVersion[OPCODE_MUL_] = 0x08;
	opcodesMinVersion[OPCODE_DIVI] = 0x08;
	opcodesMinVersion[OPCODE_DIV] = 0x08;
	opcodesMinVersion[OPCODE_DIV_] = 0x08;
	opcodesMinVersion[OPCODE_MODI] = 0x13;
	opcodesMinVersion[OPCODE_MOD] = 0x13;
	opcodesMinVersion[OPCODE_MOD_] = 0x13;
	opcodesMinVersion[OPCODE_REMI] = 0x13;
	opcodesMinVersion[OPCODE_REM] = 0x13;
	opcodesMinVersion[OPCODE_REM_] = 0x13;
	opcodesMinVersion[OPCODE_SHLI] = 0x08;
	opcodesMinVersion[OPCODE_SHRI] = 0x08;
	opcodesMinVersion[OPCODE_SARI] = 0x08;
	opcodesMinVersion[OPCODE_SHL] = 0x08;
	opcodesMinVersion[OPCODE_SHR] = 0x08;
	opcodesMinVersion[OPCODE_SAR] = 0x08;
	opcodesMinVersion[OPCODE_PUSH] = 0x08;
	opcodesMinVersion[OPCODE_POP] = 0x08;
	opcodesMinVersion[OPCODE_PUSHALL] = 0x08;
	opcodesMinVersion[OPCODE_POPALL] = 0x08;
	opcodesMinVersion[OPCODE_PUSHF] = 0x11;
	opcodesMinVersion[OPCODE_POPF] = 0x11;
	opcodesMinVersion[OPCODE_PAL] = 0x11;
	opcodesMinVersion[OPCODE_PAL_] = 0x11;
	opcodesMinVersion[OPCODE_NOTI] = 0x13;
	opcodesMinVersion[OPCODE_NOT] = 0x13;
	opcodesMinVersion[OPCODE_NOT_] = 0x13;
	opcodesMinVersion[OPCODE_NEGI] = 0x13;
	opcodesMinVersion[OPCODE_NEG] = 0x13;
	opcodesMinVersion[OPCODE_NEG] = 0x13;
}

void Chip16::CPU::reset()
{
	pc = 0x0000;
	sp = 0xFDF0;
	for (int i = 0; i < 16; i++)
		gpr[i] = 0x0000;
	flags = 0x00;
	waitingForVBlank = false;
}

void Chip16::CPU::setPc(u16 newPc)
{
	pc = newPc;
}

void Chip16::CPU::step()
{
	// If the CPU is locked waiting for the VBlank, do nothing
	if (waitingForVBlank)
		return;

	// Fetch instruction
	Instruction ins;
	ins.value = machine.getMemory().load<u32>(pc);
	pc += 4;

	// Check if the current machine version supports the current opcode
	if (opcodesMinVersion[ins.opcode()] == -1 ||
	    opcodesMinVersion[ins.opcode()] > machine.getVersion())
	{
		throw std::runtime_error(format(
			"Can't execute instruction '%s' on machine version 0x%.2X.",
			ins.toString().c_str(), machine.getVersion()));
	}

	// Run instruction
	runInstruction(ins);
}

bool Chip16::CPU::isWaitingForVBlank()
{
	return waitingForVBlank;
}

void Chip16::CPU::notifyVBlank()
{
	waitingForVBlank = false;
}

void Chip16::CPU::runInstruction(Instruction ins)
{
#define OPCODE ins.opcode()
#define REGX gpr[ins.regX()]
#define REGY gpr[ins.regY()]
#define REGZ gpr[ins.regZ()]
#define IMM ins.immediate()
#define IMM8 ins.immediate8()
#define LOW ins.low()
#define HIGH ins.high()
#define AMOUNT ins.N()
#define CONDITIONALTYPE ins.conditionalType()

	switch (OPCODE)
	{
	case OPCODE_NOP: break;
	case OPCODE_CLS: machine.getGPU().clear(); break;
	case OPCODE_VBLNK: waitingForVBlank = true; break;
	case OPCODE_BGC: machine.getGPU().setBackgroundColor(AMOUNT); break;
	case OPCODE_SPR: machine.getGPU().setSpriteSize(2*LOW, HIGH); break;
	case OPCODE_DRW: opDrw(REGX, REGY, IMM); break;
	case OPCODE_DRW_: opDrw(REGX, REGY, REGZ); break;
	// The RNG is seeded in the machine reset code
	case OPCODE_RND: REGX = rand() % (IMM + 1); break;

	case OPCODE_FLIP: machine.getGPU().setSpriteFlip(
						  (HIGH & 2) != 0, (HIGH & 1) != 0); break;

	case OPCODE_SND0: machine.getSPU().stop(); break;
	case OPCODE_SND1: machine.getSPU().playTone(500, IMM / 1000.0); break;
	case OPCODE_SND2: machine.getSPU().playTone(1000, IMM / 1000.0); break;
	case OPCODE_SND3: machine.getSPU().playTone(1500, IMM / 1000.0); break;
	case OPCODE_SNP: machine.getSPU().playWave(
		machine.getMemory().load<u16>(REGX), IMM / 1000.0); break;
	case OPCODE_SNG: machine.getSPU().setWaveParameters(
		(IMM >> 8) & 0x3, // Wave type index (T)
		(IMM8 >> 4) & 0xF, // Attack time index (A)
		(IMM8 >> 0) & 0xF, // Decay time idnex (D)
		(IMM >> 0) & 0xF, // Release time index (R)
		(IMM >> 12) & 0xF, // Peak volume index (V)
		(IMM >> 4) & 0xF // Sustain volume index (S)
	); break;

	case OPCODE_JMPI: opJmp(IMM); break;
	case OPCODE_Jx: if (isConditionalTrue(CONDITIONALTYPE)) opJmp(IMM); break;
	case OPCODE_JMC: if (haveFlag(FLAG_CARRY)) opJmp(IMM); break;
	case OPCODE_JME: if (REGX == REGY) opJmp(IMM); break;
	case OPCODE_CALLI: opCall(IMM); break;
	case OPCODE_RET: opRet(); break;
	case OPCODE_JMP: opJmp(REGX); break;
	case OPCODE_Cx: if (isConditionalTrue(CONDITIONALTYPE)) opCall(IMM); break;
	case OPCODE_CALL: opCall(REGX); break;

	case OPCODE_LDI: REGX = IMM; break;
	case OPCODE_LDISP: sp = IMM; break;
	case OPCODE_LDMI: REGX = machine.getMemory().load<u16>(IMM); break;
	case OPCODE_LDM: REGX = machine.getMemory().load<u16>(REGY); break;
	case OPCODE_MOV: REGX = REGY; break;

	case OPCODE_STMI: machine.getMemory().store<u16>(IMM, REGX); break;
	case OPCODE_STM: machine.getMemory().store<u16>(REGY, REGX); break;

	case OPCODE_ADDI: REGX = opAdd(REGX, IMM); break;
	case OPCODE_ADD: REGX = opAdd(REGX, REGY); break;
	case OPCODE_ADD_: REGZ = opAdd(REGX, REGY); break;

	case OPCODE_SUBI: REGX = opSub(REGX, IMM); break;
	case OPCODE_SUB: REGX = opSub(REGX, REGY); break;
	case OPCODE_SUB_: REGZ = opSub(REGX, REGY); break;

	case OPCODE_CMPI: opSub(REGX, IMM); break;
	case OPCODE_CMP: opSub(REGX, REGY); break;

	case OPCODE_ANDI: REGX = opAnd(REGX, IMM); break;
	case OPCODE_AND: REGX = opAnd(REGX, REGY); break;
	case OPCODE_AND_: REGZ = opAnd(REGX, REGY); break;

	case OPCODE_TSTI: opAnd(REGX, IMM); break;
	case OPCODE_TST: opAnd(REGX, REGY); break;

	case OPCODE_ORI: REGX = opOr(REGX, IMM); break;
	case OPCODE_OR: REGX = opOr(REGX, REGY); break;
	case OPCODE_OR_: REGZ = opOr(REGX, REGY); break;

	case OPCODE_XORI: REGX = opXor(REGX, IMM); break;
	case OPCODE_XOR: REGX = opXor(REGX, REGY); break;
	case OPCODE_XOR_: REGZ = opXor(REGX, REGY); break;

	case OPCODE_MULI: REGX = opMul(REGX, IMM); break;
	case OPCODE_MUL: REGX = opMul(REGX, REGY); break;
	case OPCODE_MUL_: REGZ = opMul(REGX, REGY); break;

	case OPCODE_DIVI: REGX = opDiv(REGX, IMM); break;
	case OPCODE_DIV: REGX = opDiv(REGX, REGY); break;
	case OPCODE_DIV_: REGZ = opDiv(REGX, REGY); break;

	case OPCODE_MODI: REGX = opMod(REGX, IMM); break;
	case OPCODE_MOD: REGX = opMod(REGX, REGY); break;
	case OPCODE_MOD_: REGZ = opMod(REGX, REGY); break;

	case OPCODE_REMI: REGX = opRem(REGX, IMM); break;
	case OPCODE_REM: REGX = opRem(REGX, REGY); break;
	case OPCODE_REM_: REGZ = opRem(REGX, REGY); break;

	case OPCODE_SHLI: REGX = opShl(REGX, AMOUNT); break;
	case OPCODE_SHRI: REGX = opShr(REGX, AMOUNT); break;
	case OPCODE_SARI: REGX = opSar(REGX, AMOUNT); break;
	case OPCODE_SHL:  REGX = opShl(REGX, REGY & 0xF); break;
	case OPCODE_SHR:  REGX = opShr(REGX, REGY & 0xF); break;
	case OPCODE_SAR:  REGX = opSar(REGX, REGY & 0xF); break;

	case OPCODE_PUSH: opPush(REGX); break;
	case OPCODE_POP: REGX = opPop(); break;
	// The order of PUSHALL and POPALL is not documented, but the same as
	// mash16 and RefChip16.
	case OPCODE_PUSHALL: for (size_t i = 0; i < 16; i++) opPush(gpr[i]); break;
	case OPCODE_POPALL: for (size_t i = 16; i > 0; i--) gpr[i-1] = opPop(); break;
	case OPCODE_PUSHF: opPush((u16)flags); break;
	case OPCODE_POPF: flags = (u8)opPop(); break;

	case OPCODE_PAL: machine.getGPU().loadPalette(IMM); break;
	case OPCODE_PAL_: machine.getGPU().loadPalette(REGX); break;

	case OPCODE_NOTI: REGX = opNot(IMM); break;
	case OPCODE_NOT: REGX = opNot(REGX); break;
	case OPCODE_NOT_: REGX = opNot(REGY); break;

	case OPCODE_NEGI: REGX = opNeg(IMM); break;
	case OPCODE_NEG: REGX = opNeg(REGX); break;
	case OPCODE_NEG_: REGX = opNeg(REGY); break;

	default:
		throw std::runtime_error(format(
			"Unimplemented instruction '%s'.", ins.toString().c_str()));
	}

#undef OPCODE
#undef REGX
#undef REGY
#undef REGZ
#undef IMM
#undef IMM8
#undef LOW
#undef HIGH
#undef AMOUNT
}

bool Chip16::CPU::haveFlag(u8 flag)
{
	return (flags & flag) != 0;
}

void Chip16::CPU::setFlag(u8 flag, bool newState)
{
	flags &= ~flag; // Clear flag
	if (newState == true)
		flags |= flag;
}

bool Chip16::CPU::isConditionalTrue(u8 conditionalType)
{
	switch (conditionalType)
	{
	case CONDITIONALTYPE_ZERO:
		return haveFlag(FLAG_ZERO);
	case CONDITIONALTYPE_NOT_ZERO:
		return !haveFlag(FLAG_ZERO);
	case CONDITIONALTYPE_NEGATIVE:
		return haveFlag(FLAG_SIGN);
	case CONDITIONALTYPE_NOT_NEGATIVE:
		return !haveFlag(FLAG_SIGN);
	case CONDITIONALTYPE_POSITIVE:
		return !haveFlag(FLAG_SIGN) && !haveFlag(FLAG_ZERO);

	case CONDITIONALTYPE_OVERFLOW:
		return haveFlag(FLAG_OVERFLOW);
	case CONDITIONALTYPE_NOT_OVERFLOW:
		return !haveFlag(FLAG_OVERFLOW);

	case CONDITIONALTYPE_UNSIGNED_GREATER_THAN:
		return !haveFlag(FLAG_CARRY) && !haveFlag(FLAG_ZERO);
	case CONDITIONALTYPE_UNSIGNED_GREATER_THAN_OR_EQUAL:
		return !haveFlag(FLAG_CARRY);
	case CONDITIONALTYPE_UNSIGNED_LESS_THAN:
		return haveFlag(FLAG_CARRY);
	case CONDITIONALTYPE_UNSIGNED_LESS_THAN_OR_EQUAL:
		return haveFlag(FLAG_CARRY) || haveFlag(FLAG_ZERO);

	case CONDITIONALTYPE_SIGNED_GREATER_THAN:
		return haveFlag(FLAG_OVERFLOW) == haveFlag(FLAG_SIGN) &&
			   !haveFlag(FLAG_ZERO);
	case CONDITIONALTYPE_SIGNED_GREATER_THAN_OR_EQUAL:
		return haveFlag(FLAG_OVERFLOW) == haveFlag(FLAG_SIGN);
	case CONDITIONALTYPE_SIGNED_LESS_THAN:
		return haveFlag(FLAG_OVERFLOW) != haveFlag(FLAG_SIGN);
	case CONDITIONALTYPE_SIGNED_LESS_THAN_OR_EQUAL:
		return haveFlag(FLAG_OVERFLOW) != haveFlag(FLAG_SIGN) ||
			   haveFlag(FLAG_ZERO);

	default:
		throw std::runtime_error(format(
			"Unknown conditional type %.1X!\n", conditionalType));
	}
}

void Chip16::CPU::setCarry(u32 result)
{
	setFlag(FLAG_CARRY, result >= 0x10000);
}

void Chip16::CPU::setZero(u32 result)
{
	setFlag(FLAG_ZERO, result == 0);
}

void Chip16::CPU::setSign(u32 result)
{
	setFlag(FLAG_SIGN, (result & 0x8000) != 0);
}

void Chip16::CPU::opDrw(u16 x, u16 y, u16 address)
{
	bool collision = machine.getGPU().drawSprite(x, y, address);

	setFlag(FLAG_CARRY, collision);
}

void Chip16::CPU::opJmp(u16 address)
{
	pc = address;
}

void Chip16::CPU::opCall(u16 address)
{
	opPush(pc);
	pc = address;
}

void Chip16::CPU::opRet()
{
	pc = opPop();
}

void Chip16::CPU::opPush(u16 value)
{
	machine.getMemory().store<u16>(sp, value);
	sp += 2;
}

u16 Chip16::CPU::opPop()
{
	sp -= 2;
	return machine.getMemory().load<u16>(sp);
}

u16 Chip16::CPU::opAdd(u16 op1, u16 op2)
{
	u32 result = (u16)op1 + (u16)op2;
	setCarry(result);
	setZero(result);
	setSign(result);

	setFlag(FLAG_OVERFLOW, ((s16)result > 0 && (s16)op1 < 0 && (s16)op2 < 0) ||
						   ((s16)result < 0 && (s16)op1 > 0 && (s16)op2 > 0));

	return (u16)result;
}

u16 Chip16::CPU::opSub(u16 op1, u16 op2)
{
	u32 result = (u16)op1 - (u16)op2;
	setCarry(result);
	setZero(result);
	setSign(result);

	setFlag(FLAG_OVERFLOW, ((s16)result > 0 && (s16)op1 < 0 && (s16)op2 > 0) ||
						   ((s16)result < 0 && (s16)op1 > 0 && (s16)op2 < 0));

	return (u16)result;
}

u16 Chip16::CPU::opAnd(u16 op1, u16 op2)
{
	u16 result = op1 & op2;
	setZero(result);
	setSign(result);
	return result;
}

u16 Chip16::CPU::opOr(u16 op1, u16 op2)
{
	u16 result = op1 | op2;
	setZero(result);
	setSign(result);
	return result;
}

u16 Chip16::CPU::opXor(u16 op1, u16 op2)
{
	u16 result = op1 ^ op2;
	setZero(result);
	setSign(result);
	return result;
}

u16 Chip16::CPU::opMul(u16 op1, u16 op2)
{
	u32 result = (s16)op1 * (s16)op2;
	setCarry(result);
	setZero(result);
	setSign(result);
	return (u16)result;
}

u16 Chip16::CPU::opDiv(u16 op1, u16 op2)
{
	// Use std::div because % operator is implementation-defined in C++03
	div_t res = std::div((s16)op1, (s16)op2);
	s16 result = (s16)res.quot;

	setFlag(FLAG_CARRY, ((s16)op1 % (s16)op2) != 0);
	setZero(result);
	setSign(result);
	return (u16)result;
}

u16 Chip16::CPU::opMod(u16 op1, u16 op2)
{
	// This is equivalent to RefChip16's / Haskell's `mod` implementation

	// Use std::div because % operator is implementation-defined in C++03
	div_t res = std::div((s16)op1, (s16)op2);
	s16 result = (s16)res.rem;
	if ((op1 & 0x8000) != (op2 & 0x8000))
		result += (s16)op2;

	setZero(result);
	setSign(result);
	return (u16)result;
}

u16 Chip16::CPU::opRem(u16 op1, u16 op2)
{
	// This is equivalent to RefChip16's / Haskell's `rem` implementation

	// Use std::div because % operator is implementation-defined in C++03
	div_t res = std::div((s16)op1, (s16)op2);
	s16 result = (s16)res.rem;
	setZero(result);
	setSign(result);
	return (u16)result;
}

u16 Chip16::CPU::opShl(u16 op1, u16 op2)
{
	u16 result = op1 << op2;
	setZero(result);
	setSign(result);
	return result;
}

u16 Chip16::CPU::opShr(u16 op1, u16 op2)
{
	u16 result = op1 >> op2;
	setZero(result);
	setSign(result);
	return result;
}

u16 Chip16::CPU::opSar(u16 op1, u16 op2)
{
	u16 result = (u16)((s16)op1 >> op2);
	setZero(result);
	setSign(result);
	return result;
}

u16 Chip16::CPU::opNot(u16 op)
{
	u16 result = ~op;
	setZero(result);
	setSign(result);
	return result;
}

u16 Chip16::CPU::opNeg(u16 op)
{
	s16 result = -(s16)op;
	setZero(result);
	setSign(result);
	return (u16)result;
}
