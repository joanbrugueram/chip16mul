/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Machine.hpp"

Chip16::Input::Input(Machine &machine)
	: machine(machine)
{
}

void Chip16::Input::reset()
{
}

void Chip16::Input::query()
{
	u16 keys1 = 0, keys2 = 0;
	machine.getFrontend().queryInput(keys1, keys2);
	machine.getMemory().store<u16>(0xFFF0, keys1);
	machine.getMemory().store<u16>(0xFFF2, keys2);
}
