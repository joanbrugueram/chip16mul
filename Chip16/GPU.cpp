/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Machine.hpp"
#include <cstring>
#include <vector>

// R,G,B,A values for the default palette
// [0] is transparent in the foreground layer
const static u8 defaultPalette[16][4] = {
	{0x00, 0x00, 0x00, 0xFF},
	{0x00, 0x00, 0x00, 0xFF},
	{0x88, 0x88, 0x88, 0xFF},
	{0xBF, 0x39, 0x32, 0xFF},
	{0xDE, 0x7A, 0xAE, 0xFF},
	{0x4C, 0x3D, 0x21, 0xFF},
	{0x90, 0x5F, 0x25, 0xFF},
	{0xE4, 0x94, 0x52, 0xFF},
	{0xEA, 0xD9, 0x79, 0xFF},
	{0x53, 0x7A, 0x3B, 0xFF},
	{0xAB, 0xD5, 0x4A, 0xFF},
	{0x25, 0x2E, 0x38, 0xFF},
	{0x00, 0x46, 0x7F, 0xFF},
	{0x68, 0xAB, 0xCC, 0xFF},
	{0xBC, 0xDE, 0xE4, 0xFF},
	{0xFF, 0xFF, 0xFF, 0xFF}
};

Chip16::GPU::GPU(Machine &machine)
	: machine(machine)
{
}

void Chip16::GPU::reset()
{
	// Those aren't specified at the spec
	spriteWidth = 4;
	spriteHeight = 4;
	spriteFlipX = false;
	spriteFlipY = false;
	std::memcpy(currentPalette, defaultPalette, sizeof(defaultPalette));

	clear();
}

void Chip16::GPU::clear()
{
	backgroundColor = 0;
	std::memset(screen, 0, SCREEN_WIDTH * SCREEN_HEIGHT);
}

void Chip16::GPU::render()
{
	std::vector<u8> rgbData(SCREEN_WIDTH * SCREEN_HEIGHT * 4);

	u8 *ptr = &rgbData[0];
	for (int y = 0; y < SCREEN_HEIGHT; y++)
	{
		for (int x = 0; x < SCREEN_WIDTH; x++)
		{
			u8 clr = screen[y][x] ? screen[y][x] : backgroundColor;
			for (int c = 0; c < 4; c++)
				*ptr++ = currentPalette[clr][c];
		}
	}

	machine.getFrontend().renderCallback(&rgbData[0]);
}

void Chip16::GPU::setBackgroundColor(u8 color)
{
	backgroundColor = color;
}

void Chip16::GPU::setSpriteSize(u8 width, u8 height)
{
	spriteWidth = width;
	spriteHeight = height;
}

void Chip16::GPU::setSpriteFlip(bool horizontal, bool vertical)
{
	spriteFlipX = horizontal;
	spriteFlipY = vertical;
}

#include <iostream>

void Chip16::GPU::loadPalette(u16 address)
{
	u16 currAddress = address;

	// The specified address contains the RGB (0xRR 0xGG 0xBB) values for
	// each value in the palette, consecutively
	for (int cidx = 0; cidx < 16; cidx++)
	{
		for (int comp = 0; comp < 3; comp++)
		{
			currentPalette[cidx][comp] = machine.getMemory().load<u8>(currAddress);
			currAddress++;
		}
	}
}

bool Chip16::GPU::drawSprite(u16 posX, u16 posY, u16 address)
{
	u16 currAddress = address;
	bool collision = false;

	for (int y = 0; y < spriteHeight; y++)
	{
		for (int x = 0; x < spriteWidth; x++)
		{
			// Read high nibble, then low nibble and increment.
			u8 pixelValue;
			if (x & 1)
			{
				pixelValue = machine.getMemory().load<u8>(currAddress) & 0xF;
				currAddress++;
			}
			else
			{
				pixelValue = machine.getMemory().load<u8>(currAddress) >> 4;
			}

			int theX = spriteFlipX ? (posX+spriteWidth-x-1) : (posX+x);
			int theY = spriteFlipY ? (posY+spriteHeight-y-1) : (posY+y);

			if (theY < SCREEN_HEIGHT && theX < SCREEN_WIDTH && pixelValue)
			{
				if (screen[theY][theX])
					collision = true;
				screen[theY][theX] = pixelValue;
			}
		}
	}

	return collision;
}
