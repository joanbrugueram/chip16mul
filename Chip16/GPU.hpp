/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CHIP16_GPU_HPP
#define CHIP16_GPU_HPP

#include "Machine.hpp"

namespace Chip16
{
	/**
	  * The graphics processing unit of a Chip16 machine.
	  */
	class GPU
	{
	public:
		/**
		  * The width of the virtual screen.
		  */
		static const int SCREEN_WIDTH = 320;

		/**
		  * The height of the virtual screen.
		  */
		static const int SCREEN_HEIGHT = 240;

		/**
		  * Create a new graphics processing unit.
		  * @param machine The machine which contains the GPU.
		  */
		GPU(Machine &machine);

		/**
		  * Set the CPU to its initial state.
		  */
		void reset();

		/**
		  * Clear the virtual screen.
		  */
		void clear();

		/**
		  * Render the virtual screen (calls the frontend).
		  */
		void render();

		/**
		  * Set the background color.
		  * @param color The index in the GPU palette.
		  */
		void setBackgroundColor(u8 color);

		/**
		  * Set the size of the sprites to be drawn by drawSprite().
		  * @param width The width of the sprites.
		  * @param height The height of the sprites.
		  */
		void setSpriteSize(u8 width, u8 height);

		/**
		  * Set the flipping options of drawSprite().
		  * @param horizontal To flip sprites horizontally.
		  * @param vertical To flip sprites vertically.
		  */
		void setSpriteFlip(bool horizontal, bool vertical);

		/**
		  * Load a new palette from the specified address.
		  * @param address The address of the palette data.
		  */
		void loadPalette(u16 address);

		/**
		  * Draw an sprite to the virtual screen.
		  * @param posX The X coordinate.
		  * @param posY The Y coordinate.
		  * @param address The address of the sprite data.
		  * @remarks See setSpriteSize() and setSpriteFlip().
		  */
		bool drawSprite(u16 posX, u16 posY, u16 address);

	private:
		Machine &machine;

		u8 spriteWidth, spriteHeight;
		bool spriteFlipX, spriteFlipY;
		u8 currentPalette[16][4];

		u8 backgroundColor;
		u8 screen[SCREEN_HEIGHT][SCREEN_WIDTH];
	};
}

#endif // CHIP16_GPU_HPP
