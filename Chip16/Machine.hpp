/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CHIP16_MACHINE_HPP
#define CHIP16_MACHINE_HPP

#include "Util.hpp"

namespace Chip16
{
	// Forward declare so the components can store a reference.
	class Machine;
}

#include "IFrontend.hpp"
#include "Memory.hpp"
#include "CPU.hpp"
#include "GPU.hpp"
#include "SPU.hpp"
#include "Input.hpp"

namespace Chip16
{
	/**
	  * A Chip16 machine.
	  */
	class Machine
	{
		/**
		  * Maximum supported version of the Chip16 machine, in 0xHL format as in
		  * the ROM header.
		  */
		static const u8 MACHINE_MAX_SUPPORTED_VERSION = 0x13;

		/**
		  * Version of the Chip16 machine, in 0xHL format as in the ROM header.
		  * A value of -1 means that the version of the machine has not been defined.
		  */
		int version;

		/**
		 * Number of frames that have happened since the start of the machine.
		 */
		size_t nFrames;

		/**
		  * Number of CPU cycles that have happened since the start of
			* the current frame.
			*/
		size_t cyclesIntoFrame;

		Memory memory;
		CPU cpu;
		GPU gpu;
		SPU spu;
		Input input;

		IFrontend &frontend;

	public:
		/**
		  * The number of frames to be executed in a second.
		  */
		static const int FRAMES_PER_SECOND = 60;

		/**
		  * Create a Chip16 machine.
		  * @param frontend A reference to an instance that
		  *                 implements the frontend interface.
		  */
		Machine(IFrontend &frontend);

		/**
		  * Reset the machine to its initial status.
		  */
		void reset();

		/**
		  * Load a ROM into the machine.
		  * @param rom The ROM data.
		  * @param romSize The size, in bytes, or the ROM data.
		  */
		void loadRom(const u8 *rom, size_t size);

		/**
		  * Run the machine for 1 frame (VBlank). Calls the frontend.
		  */
		void runFrame();

		/**
		  * Get the currently running version of the Chip16 machine,
			* in 0xHL format as in the ROM header.
			* A value of -1 specifies that no version is defined.
			*/
		int getVersion();

		/**
		  * Get the time, in seconds, since the machine started up.
			*/
		double getRunningTime();

		/**
		  * Get a reference to the memory chip.
		  */
		Memory &getMemory();

		/**
		  * Get a reference to the CPU.
		  */
		CPU &getCPU();

		/**
		  * Get a reference to the GPU.
		  */
		GPU &getGPU();

		/**
		  * Get a reference to the SPU.
		  */
		SPU &getSPU();

		/**
		  * Get a reference to the input device.
		  */
		Input &getInput();

		/**
		  * Get a reference to the frontend interface.
		  */
		IFrontend &getFrontend();
	};
}

#endif // CHIP16_MACHINE_HPP
