/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Machine.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include "crc32/crc.h"

Chip16::Machine::Machine(IFrontend &frontend)
	: memory(*this), cpu(*this), gpu(*this), spu(*this), input(*this),
	  frontend(frontend)
{
	reset();
}

void Chip16::Machine::reset()
{
	version = -1;
	nFrames = 0;
	cyclesIntoFrame = 0;
	memory.reset();
	cpu.reset();
	gpu.reset();
	spu.reset();
	input.reset();

	// Seed the random number generator
	// If savestates are ever implemented, we need to be careful about this piece
	// of state and probably get a RNG library, but it's perfectly OK for now.
	srand((unsigned int)time(NULL));
}

void Chip16::Machine::loadRom(const u8 *rom, size_t size)
{
	std::cout << "Loading ROM..." << std::endl;

	// Here we distinguish between two different ROM formats:
	// - If the header has magic "CH16", assume it's a ROM with header
	// https://github.com/chip16/chip16/wiki/Machine-Specification#chip16-format
	// - Otherwise, assume it is a raw ROM (load into memory and start at PC 0)
	if (size >= 16 && rom[0] == 'C' && rom[1] == 'H' && rom[2] == '1' && rom[3] == '6')
	{
		// Parse ROM header (in little endian format)
		if (rom[4] != 0x00)
		{
			throw std::runtime_error(format(
				"Invalid ROM: Invalid value '%.2X' for ROM reserved field at 0x04.", rom[4]));
		}

		u8 requiredMachineVersion = rom[5];
		u32 romSize = (rom[6]) | (rom[7] << 8) | (rom[8] << 16) | (rom[9] << 24);
		u16 startPc = (rom[10]) | (rom[11] << 8);
		u32 expectedRomCrc = (rom[12]) | (rom[13] << 8) | (rom[14] << 16) | (rom[15] << 24);

		// Load ROM into memory
		if (size-16 < romSize)
		{
			throw std::runtime_error(
				"Invalid ROM: ROM file is too small for ROM size in header.");
		}

		if (requiredMachineVersion > MACHINE_MAX_SUPPORTED_VERSION)
		{
			throw std::runtime_error(
				"Unsupported ROM: Required Chip16 machine version not supported.");
		}

		u32 actualRomCrc = crc32buf((char *)&rom[16], romSize);
		if (expectedRomCrc != actualRomCrc)
		{
			throw std::runtime_error(
				"Invalid ROM: Invalid ROM CRC32 in the header.");
		}

		version = requiredMachineVersion;
		memory.store(0x0000, &rom[16], romSize);
		cpu.setPc(startPc);

		std::cout << "Type: Headerful" << std::endl;
		std::cout << format("ROM spec version: %.2X", requiredMachineVersion) << std::endl;
		std::cout << format("ROM size: %.8X", romSize) << std::endl;
		std::cout << format("Start PC: %.4X", startPc) << std::endl;
		std::cout << format("ROM CRC32: %.8X", expectedRomCrc) << std::endl;
		std::cout << "Loaded ROM (size=" << romSize << ")" << std::endl;
	}
	else
	{
		if (size > (size_t)0xFDF0)
		{
			throw std::runtime_error(
				"Invalid ROM: ROM is too big to fit in memory.");
		}

		version = MACHINE_MAX_SUPPORTED_VERSION;
		memory.store(0x0000, rom, size);
		cpu.setPc(0x0000);

		std::cout << "Type: Headerless" << std::endl;
		std::cout << "Loaded ROM (size=" << size << ")" << std::endl;
	}
}

void Chip16::Machine::runFrame()
{
	// Step the CPU through the frame
	// Since 1 instruction = 1 cycle, we execute nCycles instructions
	// In order to get the same behaviour as Mash16, we need to run 16.667 Hz
	// per frame, hence the use of the rounding function here
	size_t nCycles = (size_t)round((double)Chip16::CPU::CYCLES_PER_SECOND / FRAMES_PER_SECOND);
	for (; cyclesIntoFrame < nCycles; cyclesIntoFrame++)
	{
		cpu.step();
		if (cpu.isWaitingForVBlank())
		{
			cyclesIntoFrame = nCycles;
			break;
		}
	}
	cpu.notifyVBlank();

	// Update per-frame components
	gpu.render();
	spu.updateStream();
	input.query();

	// Advance counters to next frame
	nFrames++;
	cyclesIntoFrame = 0;
}

int Chip16::Machine::getVersion()
{
	return version;
}

double Chip16::Machine::getRunningTime()
{
	return (nFrames + cyclesIntoFrame / (Chip16::CPU::CYCLES_PER_SECOND / (double)FRAMES_PER_SECOND))
		* (1.0 / FRAMES_PER_SECOND);
}

Chip16::Memory &Chip16::Machine::getMemory()
{
	return memory;
}

Chip16::CPU &Chip16::Machine::getCPU()
{
	return cpu;
}

Chip16::GPU &Chip16::Machine::getGPU()
{
	return gpu;
}

Chip16::SPU &Chip16::Machine::getSPU()
{
	return spu;
}

Chip16::Input &Chip16::Machine::getInput()
{
	return input;
}

Chip16::IFrontend &Chip16::Machine::getFrontend()
{
	return frontend;
}
