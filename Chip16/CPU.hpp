/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CHIP16_CPU_HPP
#define CHIP16_CPU_HPP

#include "Machine.hpp"

namespace Chip16
{
	/**
	  * The central processing unit of a Chip16 machine.
	  */
	class CPU
	{
		Machine &machine;

		/// Program counter
		u16 pc;
		/// Stack pointer
		u16 sp;
		/// General purpose registers
		u16 gpr[16];
		/// Flags register
		u8 flags;
		/// True if the CPU is locked (no instructions executed)
		/// until the next VBlank. Set by the VBLNK instruction.
		bool waitingForVBlank;

		/**
		  * Flags of the flags register.
		  */
		enum Flags
		{
			FLAG_CARRY = 1 << 1,
			FLAG_ZERO = 1 << 2,
			FLAG_OVERFLOW = 1 << 6,
			FLAG_SIGN = 1 << 7
		};

		/**
		  * List of conditional types (for Jx and Cx opcodes).
		  * Those can be checked with isConditionalTrue().
		  */
		enum ConditionalTypes
		{
			CONDITIONALTYPE_ZERO = 0x0,
			CONDITIONALTYPE_NOT_ZERO = 0x1,
			CONDITIONALTYPE_NEGATIVE = 0x2,
			CONDITIONALTYPE_NOT_NEGATIVE = 0x3,
			CONDITIONALTYPE_POSITIVE = 0x4,

			CONDITIONALTYPE_OVERFLOW = 0x5,
			CONDITIONALTYPE_NOT_OVERFLOW = 0x6,

			CONDITIONALTYPE_UNSIGNED_GREATER_THAN = 0x7,
			CONDITIONALTYPE_UNSIGNED_GREATER_THAN_OR_EQUAL = 0x8,
			CONDITIONALTYPE_UNSIGNED_LESS_THAN = 0x9,
			CONDITIONALTYPE_UNSIGNED_LESS_THAN_OR_EQUAL = 0xA,

			CONDITIONALTYPE_SIGNED_GREATER_THAN = 0xB,
			CONDITIONALTYPE_SIGNED_GREATER_THAN_OR_EQUAL = 0xC,
			CONDITIONALTYPE_SIGNED_LESS_THAN = 0xD,
			CONDITIONALTYPE_SIGNED_LESS_THAN_OR_EQUAL = 0xE
		};

		/**
		  * Instruction operation codes.
		  */
		enum Opcodes
		{
			/* For ambiguous opcodes:
			 * I suffix: Takes an immediate
			 * _ suffix: Takes all parameters as registers */
			OPCODE_NOP = 0x00,
			OPCODE_CLS = 0x01,
			OPCODE_VBLNK = 0x02,
			OPCODE_BGC = 0x03,
			OPCODE_SPR = 0x04,
			OPCODE_DRW = 0x05,
			OPCODE_DRW_ = 0x06,
			OPCODE_RND = 0x07,

			OPCODE_FLIP = 0x08,

			OPCODE_SND0 = 0x09,
			OPCODE_SND1 = 0x0A,
			OPCODE_SND2 = 0x0B,
			OPCODE_SND3 = 0x0C,
			OPCODE_SNP = 0x0D,
			OPCODE_SNG = 0x0E,

			OPCODE_JMPI = 0x10,
			OPCODE_JMC = 0x11,
			OPCODE_Jx = 0x12,
			OPCODE_JME = 0x13,
			OPCODE_CALLI = 0x14,
			OPCODE_RET = 0x15,
			OPCODE_JMP = 0x16,
			OPCODE_Cx = 0x17,
			OPCODE_CALL = 0x18,

			OPCODE_LDI = 0x20,
			OPCODE_LDISP = 0x21,
			OPCODE_LDMI = 0x22,
			OPCODE_LDM = 0x23,
			OPCODE_MOV = 0x24,

			OPCODE_STMI = 0x30,
			OPCODE_STM = 0x31,

			OPCODE_ADDI = 0x40,
			OPCODE_ADD = 0x41,
			OPCODE_ADD_ = 0x42,

			OPCODE_SUBI = 0x50,
			OPCODE_SUB = 0x51,
			OPCODE_SUB_ = 0x52,

			OPCODE_CMPI = 0x53,
			OPCODE_CMP = 0x54,

			OPCODE_ANDI = 0x60,
			OPCODE_AND = 0x61,
			OPCODE_AND_ = 0x62,

			OPCODE_TSTI = 0x63,
			OPCODE_TST = 0x64,

			OPCODE_ORI = 0x70,
			OPCODE_OR = 0x71,
			OPCODE_OR_ = 0x72,

			OPCODE_XORI = 0x80,
			OPCODE_XOR = 0x81,
			OPCODE_XOR_ = 0x82,

			OPCODE_MULI = 0x90,
			OPCODE_MUL = 0x91,
			OPCODE_MUL_ = 0x92,

			OPCODE_DIVI = 0xA0,
			OPCODE_DIV = 0xA1,
			OPCODE_DIV_ = 0xA2,

			OPCODE_MODI = 0xA3,
			OPCODE_MOD = 0xA4,
			OPCODE_MOD_ = 0xA5,

			OPCODE_REMI = 0xA6,
			OPCODE_REM = 0xA7,
			OPCODE_REM_ = 0xA8,

			OPCODE_SHLI = 0xB0,
			OPCODE_SHRI = 0xB1,
			// OPCODE_SALI is the same as OPCODE_SHLI
			OPCODE_SARI = 0xB2,
			OPCODE_SHL = 0xB3,
			OPCODE_SHR = 0xB4,
			// OPCODE_SAL is the same as OPCODE_SHL
			OPCODE_SAR = 0xB5,

			OPCODE_PUSH = 0xC0,
			OPCODE_POP = 0xC1,
			OPCODE_PUSHALL = 0xC2,
			OPCODE_POPALL = 0xC3,
			OPCODE_PUSHF = 0xC4,
			OPCODE_POPF = 0xC5,

			OPCODE_PAL = 0xD0,
			OPCODE_PAL_ = 0xD1,

			OPCODE_NOTI = 0xE0,
			OPCODE_NOT = 0xE1,
			OPCODE_NOT_ = 0xE2,

			OPCODE_NEGI = 0xE3,
			OPCODE_NEG = 0xE4,
			OPCODE_NEG_ = 0xE5
		};

		/**
		  * Minimum version of the Chip16 machine (in 0xHL format, as in the
			* ROM header) required to execute each opcode.
			*
			* This will be -1 for unsupported instructions.
			*/
		int opcodesMinVersion[0x100];

		/**
		  * Wrapper for instruction which provides utility methods.
		  */
		struct Instruction
		{
			u32 value;

			inline u8 opcode() { return value & 0xFF; }
			inline u16 immediate() { return (value >> 16) & 0xFFFF; }
			inline u8 immediate8() { return (value >> 8) & 0xFF; }
			inline u8 low() { return (value >> 16) & 0xFF;}
			inline u8 high() { return (value >> 24) & 0xFF; }
			inline u8 regX() { return (value >> 8) & 0xF; }
			inline u8 regY() { return (value >> 12) & 0xF; }
			inline u8 regZ() { return (value >> 16) & 0xF; }
			inline u8 conditionalType() { return (value >> 8) & 0xF; }
			inline u8 N() { return (value >> 16) & 0xF; }

			/**
			  * Disassemble an instruction.
			  * @returns The assembly line corresponding to the instruction.
			  */
			std::string toString();
		};


	public:
		/**
		  * Number of cycles that the CPU executes on a second.
		  */
		static const int CYCLES_PER_SECOND = 1000000; // 1 MHz

		/**
		  * Create a central processing unit.
		  * @param machine The machine which contains the CPU.
		  */
		CPU(Machine &machine);

		/**
		  * Set the CPU to its initial state.
		  */
		void reset();

		/**
		  * Set the program counter (PC) to the specified value.
			* @param newPc The new program counter (PC) value.
			*/
		void setPc(u16 newPc);

		/**
		  * Run for CPU until the next VBlank can be done.
		  */
		void step();

		/**
		  * Checks if the CPU is locked waiting for a VBlank to happen.
			*/
		bool isWaitingForVBlank();

		/**
		  * Notify the CPU that a VBlank has happened.
			* This unlocks the CPU if it has been previously locked by VBLNK.
			*/
		void notifyVBlank();

	private:
		/**
		  * Run a single instruction.
		  * @param ins The instruction to run.
		  */
		void runInstruction(Instruction ins);

		/**
		  * Checks if the specified flag is set in the flags register.
		  * @param flag The flag to check (see Flags).
		  */
		bool haveFlag(u8 flag);

		/**
		  * Sets or unsets the specified flag depending on newState.
		  * @param flag The flag to set or unset (see Flags).
		  * @param newState The new value of the flag.
		  */
		void setFlag(u8 flag, bool newState);

		/**
		  * Checks if the specified conditional is true.
		  * @param conditionalType A conditional type (see ConditionalTypes).
		  */
		bool isConditionalTrue(u8 conditionalType);

		/**
		  * Sets the carry flag if result >= 0x10000.
		  * @param result The result of an operation.
		  */
		void setCarry(u32 result);

		/**
		  * Sets the zero flag if result == 0.
		  * @param The result of an operation.
		  */
		void setZero(u32 result);

		/**
		  * Sets the sign flag if (result & 0x8000) != 0.
		  * @param The result of an operation.
		  */
		void setSign(u32 result);

		/**
		  * Tells the GPU to draw an sprite.
		  * @param x The X coordinate where the sprite should be drawn.
		  * @param y The Y coordinate where the sprite should be drawn.
		  * @param addr The address of the sprite data.
		  * @remarks Sets FLAG_CARRY <=> sprite collision.
		  */
		void opDrw(u16 x, u16 y, u16 address);

		/**
		  * Performs a jump.
		  * @param address The new program counter.
		  * @remarks Modifies pc.
		  */
		void opJmp(u16 address);

		/**
		  * Performs a call.
		  * @param address The new program counter.
		  * @remarks Modifies pc, sp.
		  */
		void opCall(u16 address);

		/**
		  * Returns from a call.
		  * @remarks Modifies pc, sp.
		  */
		void opRet();

		/**
		  * Pushes a value into the stack.
		  * @param value The value to push.
		  * @remarks Modifies sp.
		  */
		void opPush(u16 value);

		/**
		  * Pops a value from the stack.
		  * @returns The popped value.
		  * @remarks Modifies sp.
		  */
		u16 opPop();

		/**
		  * Adds two values.
		  * @param op1 The first addend.
		  * @param op2 The second addend.
		  * @returns The sum.
		  * @remarks Sets FLAG_CARRY, FLAG_ZERO, FLAG_OVERFLOW, FLAG_SIGN.
		  */
		u16 opAdd(u16 op1, u16 op2);

		/**
		  * Substracts two values.
		  * @param op1 The minuend.
		  * @param op2 The substrahend.
		  * @returns The difference.
		  * @remarks Sets FLAG_CARRY, FLAG_ZERO, FLAG_OVERFLOW, FLAG_SIGN.
		  */
		u16 opSub(u16 op1, u16 op2);

		/**
		  * ANDs two values.
		  * @param op1 The first operand.
		  * @param op2 The second operand.
		  * @returns The result.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opAnd(u16 op1, u16 op2);

		/**
		  * ORs two values.
		  * @param op1 The first operand.
		  * @param op2 The second operand.
		  * @returns The result.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opOr(u16 op1, u16 op2);

		/**
		  * XORs two values.
		  * @param op1 The first operand.
		  * @param op2 The second operand.
		  * @returns The result.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opXor(u16 op1, u16 op2);

		/**
		  * Multiplies two values.
		  * @param op1 The first factor.
		  * @param op2 The second factor.
		  * @returns The product.
		  * @remarks Sets FLAG_CARRY, FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opMul(u16 op1, u16 op2);

		/**
		  * Divides two values.
		  * @param op1 The dividend.
		  * @param op2 The divisor.
		  * @returns The quotient.
		  * @remarks Sets FLAG_CARRY, FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opDiv(u16 op1, u16 op2);

		/**
		  * Calculates the modulo of dividing two values.
		  * @param op1 The dividend.
		  * @param op2 The divisor.
		  * @returns The modulo.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opMod(u16 op1, u16 op2);

		/**
		  * Calculates the remainder of dividing two values.
		  * @param op1 The dividend.
		  * @param op2 The divisor.
		  * @returns The remainder.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opRem(u16 op1, u16 op2);

		/**
		  * Does a left bit shift on a value.
		  * @param op1 The value.
		  * @param op2 The number of bits to shift.
		  * @returns The result.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opShl(u16 op1, u16 op2);

		/**
		  * Does a logical right bit shift on a value.
		  * @param op1 The value.
		  * @param op2 The number of bits to shift.
		  * @returns The result.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opShr(u16 op1, u16 op2);

		/**
		  * Does an arithmetic right bit shift on a value.
		  * @param op1 The value.
		  * @param op2 The number of bits to shift.
		  * @returns The result.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opSar(u16 op1, u16 op2);

		/**
		  * Calculates the NOT (bit inversion) of a value.
		  * @param op The value.
		  * @returns The result of the NOT operation on the value.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opNot(u16 op);

		/**
		  * Calculates the negation of a value.
		  * @param op The value.
		  * @returns The negated value.
		  * @remarks Sets FLAG_ZERO, FLAG_SIGN.
		  */
		u16 opNeg(u16 op);
	};
}


#endif // CHIP16_CPU_HPP
