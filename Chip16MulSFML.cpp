/*
	Copyright (C) 2012 Joan Bruguera Micó.

	This file is part of Chip16Mul.

	Chip16Mul is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Chip16Mul is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Chip16Mul.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <fstream>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include "Chip16/Machine.hpp"

std::vector<u8> readFile(const char *path)
{
	std::ifstream file(path, std::ios::in | std::ios::binary);
	if (!file.is_open())
		throw std::runtime_error("Can't open the ROM file.");

	file.seekg(0, std::ios::end);
	std::streamoff length = file.tellg();
	file.seekg(0, std::ios::beg);

	std::vector<u8> data((size_t)length);
	file.read((char *)&data[0], data.size());
	if (file.fail())
		throw std::runtime_error("Can't read the ROM file.");

	return data;
}

class AudioBuffer : public sf::SoundStream
{
public:
	AudioBuffer(Chip16::Machine &machine, sf::Mutex &machineMutex)
		: machine(machine), machineMutex(machineMutex)
	{
		initialize(1, Chip16::SPU::AUDIO_FREQUENCY);
	}

private:
	virtual bool onGetData(Chunk& data)
	{
		// Ask machine for more samples
		machineMutex.lock();
		buffer = machine.getSPU().readStream();
		machineMutex.unlock();

		if (buffer.empty())
		{
			// No data available, stream some silence while the buffer fills
			buffer.resize(0x200);
		}

		data.samples = &buffer[0];
		data.sampleCount = buffer.size();

		// Return true to continue playing
		return true;
	}

	virtual void onSeek(sf::Time timeOffset)
	{
		if (timeOffset != sf::Time::Zero)
			throw std::runtime_error("SfmlFrontend::AudioBuffer::OnSeek");
	}

private:
	Chip16::Machine &machine;
	sf::Mutex &machineMutex;
	std::vector<s16> buffer;
};

class SfmlFrontend : public Chip16::IFrontend
{
	sf::RenderWindow window;
	AudioBuffer audioBuffer;
	Chip16::Machine machine;
	sf::Mutex machineMutex;

public:
	SfmlFrontend()
		: window(sf::VideoMode(Chip16::GPU::SCREEN_WIDTH, Chip16::GPU::SCREEN_HEIGHT), "Chip16Mul"),
		  machine(*this), audioBuffer(machine, machineMutex)
	{
		audioBuffer.play();
	}

	void mainLoop()
	{
		const float timePerFrame = 1.0f / Chip16::Machine::FRAMES_PER_SECOND;
		sf::Clock clock;
		float prevFrameTime = 0.0f;
		float timeDelay = 0.0f;

		// Start the game loop
		while (window.isOpen())
		{
			// Process events
			sf::Event event;
			while (window.pollEvent(event))
			{
				// Close window : exit
				if (event.type == sf::Event::Closed)
					window.close();
			}

			machineMutex.lock();
			machine.runFrame();
			machineMutex.unlock();

			// Cap framerate
			timeDelay += timePerFrame;

			float currentTime = clock.getElapsedTime().asSeconds();
			timeDelay -= (currentTime - prevFrameTime);
			prevFrameTime = currentTime;

			if (timeDelay > 0.0)
				sf::sleep(sf::seconds(timeDelay));
		}
	}

	void loadRom(u8 *data, size_t size)
	{
		machineMutex.lock();
		machine.loadRom(data, size);
		machineMutex.unlock();
	}

	virtual void renderCallback(u8 *rgbaData)
	{
		window.clear();

		sf::Texture tex;
		tex.create(Chip16::GPU::SCREEN_WIDTH, Chip16::GPU::SCREEN_HEIGHT);
		tex.update(rgbaData);

		window.draw(sf::Sprite(tex));

		window.display();
	}

	virtual void queryInput(u16 &keys1, u16 &keys2)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))    keys1 |= Chip16::Input::INPUT_UP;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))  keys1 |= Chip16::Input::INPUT_DOWN;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))  keys1 |= Chip16::Input::INPUT_LEFT;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) keys1 |= Chip16::Input::INPUT_RIGHT;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))     keys1 |= Chip16::Input::INPUT_SELECT;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))     keys1 |= Chip16::Input::INPUT_START;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::X))     keys1 |= Chip16::Input::INPUT_A;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))     keys1 |= Chip16::Input::INPUT_B;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8)) keys2 |= Chip16::Input::INPUT_UP;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2)) keys2 |= Chip16::Input::INPUT_DOWN;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad4)) keys2 |= Chip16::Input::INPUT_LEFT;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad6)) keys2 |= Chip16::Input::INPUT_RIGHT;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad9)) keys2 |= Chip16::Input::INPUT_SELECT;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7)) keys2 |= Chip16::Input::INPUT_START;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1)) keys2 |= Chip16::Input::INPUT_A;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3)) keys2 |= Chip16::Input::INPUT_B;
	}
};

int main(int argc, char *argv[])
{
	try
	{
		if (argc != 2)
		{
			std::cout << "Usage: " << argv[0] << " rom.c16" << std::endl;
			return EXIT_FAILURE;
		}

		std::vector<u8> rom = readFile(argv[1]);

		SfmlFrontend frontend;
		frontend.loadRom(&rom[0], rom.size());
		frontend.mainLoop();

		return EXIT_SUCCESS;
	}
	catch (const std::exception &ex)
	{
		std::cerr << ex.what() << std::endl;

		return EXIT_FAILURE;
	}
}
